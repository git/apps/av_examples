-include DIRS
-include ../../Rules.make

all: debug release

debug:
	for dir in $(DIRS); do \
                make -C $$dir debug; \
        done

release:
	for dir in $(DIRS); do \
                make -C $$dir release; \
	done

clean:
	for dir in $(DIRS); do \
                make -C $$dir clean; \
	done

install:
	@install -d $(DESTDIR)/usr/bin
	@if [ -e saDstColorkey/Release/saDstColorkey ] ; then \
		install saDstColorkey/Release/saDstColorkey $(DESTDIR)/usr/bin/ ; \
		echo "saDstColorkey release version installed."; \
	else \
		echo "saDstColorkey release version not built - nothing to install!"; \
	fi
	@if [ -e saMmapDisplay/Release/saMmapDisplay ] ; then \
		install saMmapDisplay/Release/saMmapDisplay $(DESTDIR)/usr/bin/ ; \
		echo "saMmapDisplay release version installed."; \
	else \
		echo "saMmapDisplay release version not built - nothing to install!"; \
	fi
	@if [ -e saUserPtrLoopback/Release/saUserPtrLoopback ] ; then \
		install saUserPtrLoopback/Release/saUserPtrLoopback $(DESTDIR)/usr/bin/ ; \
		echo "saUserPtrLoopback release version installed."; \
	else \
		echo "saUserPtrLoopback release version not built - nothing to install!"; \
	fi
	@if [ -e saFbdevDisplay/Release/saFbdevDisplay ] ; then \
		install saFbdevDisplay/Release/saFbdevDisplay $(DESTDIR)/usr/bin/ ; \
		echo "saFbdevDisplay release version installed."; \
	else \
		echo "saFbdevDisplay release version not built - nothing to install!"; \
	fi
	@if [ -e saMmapLoopback/Release/saMmapLoopback ] ; then \
		install saMmapLoopback/Release/saMmapLoopback $(DESTDIR)/usr/bin/ ; \
		echo "saMmapLoopback release version installed."; \
	else \
		echo "saMmapLoopback release version not built - nothing to install!"; \
	fi
	@if [ -e saV4L2Rotation/Release/saV4L2Rotation ] ; then \
		install saV4L2Rotation/Release/saV4L2Rotation $(DESTDIR)/usr/bin/ ; \
		echo "saV4L2Rotation release version installed."; \
	else \
		echo "saV4L2Rotation release version not built - nothing to install!"; \
	fi
	@if [ -e saFbdevDisplayPan/Release/saFbdevDisplayPan ] ; then \
		install saFbdevDisplayPan/Release/saFbdevDisplayPan $(DESTDIR)/usr/bin/ ; \
		echo "saFbdevDisplayPan release version installed."; \
	else \
		echo "saFbdevDisplayPan release version not built - nothing to install!"; \
	fi
	@if [ -e saScalingDisplay/Release/saScalingDisplay ] ; then \
		install saScalingDisplay/Release/saScalingDisplay $(DESTDIR)/usr/bin/ ; \
		echo "saScalingDisplay release version installed."; \
	else \
		echo "saScalingDisplay release version not built - nothing to install!"; \
	fi
	@if [ -e saVideoARGB/Release/saVideoARGB ] ; then \
		install saVideoARGB/Release/saVideoARGB $(DESTDIR)/usr/bin/ ; \
		echo "saVideoARGB release version installed."; \
	else \
		echo "saVideoARGB release version not built - nothing to install!"; \
	fi
	@if [ -e saFbdevRotation/Release/saFbdevRotation ] ; then \
		install saFbdevRotation/Release/saFbdevRotation $(DESTDIR)/usr/bin/ ; \
		echo "saFbdevRotation release version installed."; \
	else \
		echo "saFbdevRotation release version not built - nothing to install!"; \
	fi
	@if [ -e saSrcColorkey/Release/saSrcColorkey ] ; then \
		install saSrcColorkey/Release/saSrcColorkey $(DESTDIR)/usr/bin/ ; \
		echo "saSrcColorkey release version installed."; \
	else \
		echo "saSrcColorkey release version not built - nothing to install!"; \
	fi
	@if [ -e saGraphicsARGB/Release/saGraphicsARGB ] ; then \
		install saGraphicsARGB/Release/saGraphicsARGB $(DESTDIR)/usr/bin/ ; \
		echo "saGraphicsARGB release version installed."; \
	else \
		echo "saGraphicsARGB release version not built - nothing to install!"; \
	fi
	@if [ -e saUserptrDisplay/Release/saUserptrDisplay ] ; then \
                install saUserptrDisplay/Release/saUserptrDisplay $(DESTDIR)/usr/bin/ ; \
                echo "saUserptrDisplay release version installed."; \
        else \
                echo "saUserptrDisplay release version not built - nothing to install!"; \
        fi

install_debug:
	@install -d $(DESTDIR)/usr/bin
	@if [ -e saDstColorkey/Debug/saDstColorkey ] ; then \
                install saDstColorkey/Debug/saDstColorkey $(DESTDIR)/usr/bin/ ; \
                echo "saDstColorkey debug version installed."; \
	else \
                echo "saDstColorkey debug version not built - nothing to install!"; \
        fi
	@if [ -e saMmapDisplay/Debug/saMmapDisplay ] ; then \
                install saMmapDisplay/Debug/saMmapDisplay $(DESTDIR)/usr/bin/ ; \
                echo "saMmapDisplay debug version installed."; \
        else \
                echo "saMmapDisplay debug version not built - nothing to install!"; \
        fi
	@if [ -e saUserPtrLoopback/Debug/saUserPtrLoopback ] ; then \
                install saUserPtrLoopback/Debug/saUserPtrLoopback $(DESTDIR)/usr/bin/ ; \
                echo "saUserPtrLoopback debug version installed."; \
        else \
                echo "saUserPtrLoopback debug version not built - nothing to install!"; \
        fi
	@if [ -e saFbdevDisplay/Debug/saFbdevDisplay ] ; then \
                install saFbdevDisplay/Debug/saFbdevDisplay $(DESTDIR)/usr/bin/ ; \
                echo "saFbdevDisplay debug version installed."; \
        else \
                echo "saFbdevDisplay debug version not built - nothing to install!"; \
        fi
	@if [ -e saMmapLoopback/Debug/saMmapLoopback ] ; then \
                install saMmapLoopback/Debug/saMmapLoopback $(DESTDIR)/usr/bin/ ; \
                echo "saMmapLoopback debug version installed."; \
        else \
                echo "saMmapLoopback debug version not built - nothing to install!"; \
        fi
	@if [ -e saV4L2Rotation/Debug/saV4L2Rotation ] ; then \
		install saV4L2Rotation/Debug/saV4L2Rotation $(DESTDIR)/usr/bin/ ; \
                echo "saV4L2Rotation debug version installed."; \
	else \
		echo "saV4L2Rotation debug version not built - nothing to install!"; \
	fi
	@if [ -e saFbdevDisplayPan/Debug/saFbdevDisplayPan ] ; then \
               install saFbdevDisplayPan/Debug/saFbdevDisplayPan $(DESTDIR)/usr/bin/ ; \
                echo "saFbdevDisplayPan debug version installed."; \
        else \
                echo "saFbdevDisplayPan debug version not built - nothing to install!"; \
        fi
	@if [ -e saScalingDisplay/Debug/saScalingDisplay ] ; then \
                install saScalingDisplay/Debug/saScalingDisplay $(DESTDIR)/usr/bin/ ; \
                echo "saScalingDisplay debug version installed."; \
        else \
                echo "saScalingDisplay debug version not built - nothing to install!"; \
	fi
	@if [ -e saVideoARGB/Debug/saVideoARGB ] ; then \
		install saVideoARGB/Debug/saVideoARGB $(DESTDIR)/usr/bin/ ; \
		echo "saVideoARGB debug version installed."; \
	else \
		echo "saVideoARGB debug version not built - nothing to install!"; \
	fi
	@if [ -e saFbdevRotation/Debug/saFbdevRotation ] ; then \
		install saFbdevRotation/Debug/saFbdevRotation $(DESTDIR)/usr/bin/ ; \
		echo "saFbdevRotation debug version installed."; \
	else \
		echo "saFbdevRotation debug version not built - nothing to install!"; \
	fi
	@if [ -e saSrcColorkey/Debug/saSrcColorkey ] ; then \
		install saSrcColorkey/Debug/saSrcColorkey $(DESTDIR)/usr/bin/ ; \
		echo "saSrcColorkey debug version installed."; \
	else \
		echo "saSrcColorkey debug version not built - nothing to install!"; \
	fi
	@if [ -e saGraphicsARGB/Debug/saGraphicsARGB ] ; then \
                install saGraphicsARGB/Debug/saGraphicsARGB $(DESTDIR)/usr/bin/ ; \
                echo "saGraphicsARGB debug version installed."; \
        else \
	echo "saGraphicsARGB debug version not built - nothing to install!"; \
        fi
	@if [ -e saUserptrDisplay/Debug/saUserptrDisplay ] ; then \
                install saUserptrDisplay/Debug/saUserptrDisplay $(DESTDIR)/usr/bin/ ; \
                echo "saUserptrDisplay debug version installed."; \
	else \
                echo "saUserptrDisplay debug version not built - nothing to install!"; \
	fi
